# make -f Makefile_pdflatex

R = final_report
STY = *.cls
FIG =

pdf: $(R).pdf

$(R).bbl:
	pdflatex $(R)
	bibtex $(R)
	bibtex $(R)
	pdflatex $(R)

$(R).pdf: *.tex  $(R).bbl $(FIG)
	pdflatex $(R)
	bibtex $(R)
	bibtex $(R)
	pdflatex $(R)

clean:
	rm -f $(R).log *.aux $(R).bbl $(R).blg $(R).out ${R}.brf
