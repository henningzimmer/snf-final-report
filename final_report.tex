\documentclass[10pt,a4paper]{article}            % review

\usepackage[top=3cm,bottom=3cm,left=3cm,right=3cm]{geometry}

%\usepackage{times}

%% The 'graphicx' package allows for the inclusion of EPS figures.

\usepackage{graphicx}

%% use this for zero \parindent and non-zero \parskip, intelligently.

\usepackage{parskip}

\usepackage[usenames,dvipsnames]{color}

\linespread{1.25}


\usepackage{dsfont}
\usepackage{url}
\usepackage{amsmath}
\usepackage{cite}

\usepackage{longtable}

%% Definitions

\renewcommand{\vec}[1]{\mathbf{#1}}
\let\set=\mathcal
\let\texture=\mathcal

\newcommand{\squishlist}{
   \begin{list}{$\bullet$}
    { \setlength{\itemsep}{-3pt}      \setlength{\parsep}{3pt}
      \setlength{\topsep}{0pt}       \setlength{\partopsep}{0pt}
      \setlength{\leftmargin}{3em} \setlength{\labelwidth}{1em}
      \setlength{\labelsep}{0.5em} } }
\newcommand{\squishend}{
    \end{list}  }



% end each line before an equation with this command to avoid clash between amsmath and lineno packages
\newcommand{\eqnbreak}{\par\vspace{-1.2\baselineskip}}

%\renewcommand{\baselinestretch}{0.99}
\renewcommand{\topfraction}{1.0}	% max fraction of floats at top
\renewcommand{\bottomfraction}{1.0}

%%%%%% START OF THE PAPER %%%%%%

\begin{document}


%\begin{titlepage}
\title{Novel Methods for Spatio-Temporal Image Interpolation\\
	--\\
Final Scientific Report
}

\author{
Prof. Dr.\ Markus Gross (\url{grossm@inf.ethz.ch})\\
\\
Institute of Visual Computing\\
Department of Computer Science\\
ETH Z\"urich
}

\date{}

\maketitle

\newpage


\section{Summary of Conducted Research}
In this report we present the research results for the project 'Novel Methods for Spatio-Temporal Image Interpolation' w.r.t.\ the proposed research plan. The goal of this project was to develop algorithms that allow us to synthesize novel image content from acquired real-world footage in the form of monocular videos as well as multi-camera systems. In addition to the novel algorithms that advanced the state-of-the-art in this broad field of research we also captured and disseminated novel test datasets that better represent the current quality standards in actual production settings in the industry, i.e.\ high spatial resolution, large number of images, etc.

We successfully completed work packages 1-3 and 5-7 from the proposal, which encompassed implementing a common software framework, capturing high resolution input data, setting up corresponding benchmarks and performing evaluations, developing novel algorithms to handle high resolution data, as well as proposing methods that jointly integrate correspondence analysis and interpolation.

We decided to postpone the work package 4 that was supposed to investigate 'Relaxed Interpolation Techniques'
to a possible next proposal, as it seemed risky and the outcome was unclear given te remaining time in the project. 
%
We focused on video segmentation as we found that having methods that can accurately delineate foreground objects in input videos would eventually allow us to resolve challenging occlusion problems that currently seem to be the main remaining challenge for all sorts of interpolation algorithms.


In the following we describe the results of each work package in more detail.


\subsection{WP 1: Unifying Software Framework for Image Interpolation} % WP 1
We started the project by developing a unifying software framework in C++ that served as a basis for further implementation and testing of the forthcoming work packages.
In this framework we implemented basic image and video I/O functionality as well as standard interpolation algorithms. % as for example described in \cite{BSLRBS10}.
Additionally we implemented and integrated several well-established optical flow algorithms %\cite{BBPW04,WerlbergerTPWCB09,ZBW11a,XuJM12,TaoBKP12}
that served as baseline method for correspondence estimation in later experiments.


\subsection{WP2: Extension of Existing Methods to High Resolution Data} % WP2

In \cite{PerazziSZKWWG15} we presented a method to seamlessly stitch high resolution video streams from unstructured camera arrays into a single ultra high resolution panoramic video that can for example be experienced when projected into a large dome, see Fig.\ref{pano-teaser} (left) for an example.
The main challenge here was to overcome parallax issues that result in ghosting artifacts in an initial homography-based alignment. To this end we compute optical flow in the overlap regions and warped the images correspondingly to interpolate a parallax-free in-between view (see Fig.\ref{pano-teaser} (right)). As the optimal order in which images are warped has a big influence on the result quality, we proposed an efficient algorithm to find the optimal warp order. To achieve temporally stable results we proposed to do per-frame computations that are restricted to stay close to the initial alignment, as a global solution is not possible for large video volumes.

\begin{figure}[t]
	\centering
	\includegraphics[width=.74\linewidth]{images/pano-teaser}
	\hspace*{2mm}
	\includegraphics[width=.22\linewidth]{images/pano-flow}
	\caption{In \cite{PerazziSZKWWG15} we showed how to create panoramic video from unstructured camera arrays (left) using optical flow-based parallax compensation by synthesizing in-between viewpoints (right).}
	\label{pano-teaser}
\end{figure}


In \cite{KimZPSG13} we presented a highly efficient method for computing dense depth maps from densely sampled, high-resolution light field input (Fig.~\ref{dflf} (left to middle right)). We could show that standard stereo and multi-view depth estimation approaches such as graph cuts or variational stereo do not scale well for such data and even worse produce inferior results. Our method refrains from any kind of global optimization and works on a per-pixel bases, making it very efficient and parallelizable. More importantly, our method also allows us to obtain very accurate reconstructions around object silhouettes, which is especially important for application of depth maps in interpolation tasks in image-based rendering; see Fig.~\ref{dflf} (right).

\begin{figure}[t]
	\centering
	\includegraphics[width=.24\linewidth]{images/dflf-mansion}
	\includegraphics[width=.24\linewidth]{images/dflf-mansion-depth}
	\includegraphics[width=.24\linewidth]{images/dflf-mansion-depth-zoom}
	\includegraphics[width=.25\linewidth]{images/dflf-mansion-ibr}
	\caption{In \cite{KimZPSG13} we proposed a method to compute accurate dense depth maps from densely sampled high resolution light field input (left to middle right). This can serve as input for interpolating novel viewpoints (right).}
	\label{dflf}
\end{figure}


A remaining issue for our depth estimation method was noise in homogeneous image regions that can lead to spurious artifacts in the interpolated results. In \cite{WolffKZSBSS16} we propose a simple, yet effective noise reduction method that leverages the redundancy in depth maps computed from the many views in the light field. See Fig.~\ref{denoise} for an example.

\begin{figure}[t]
	\centering
	\includegraphics[width=.99\linewidth]{images/denoise-teaser}
	\caption{In \cite{WolffKZSBSS16} we propose a method to remove noise and outliers from point clouds obtained using image-based 3D reconstruction techniques, such as our light field depth estimation technique \cite{KimZPSG13}. 
		As can be seen, using the noisy point cloud (b) as input for a common surface reconstruction technique results in disturbing artifacts in the final 3D mesh reconstruction (c), whereas our filtered point cloud (d) allows to obtain a favorable surface reconstruction (e).}
	\label{denoise}
\end{figure} 

In \cite{Oztireli15Perceptual}, we addressed the fundamental problem of downscaling high resolution images. This is the first algorithm that takes perception into account for this problem. The algorithm allows accurate reproductions of high resolution images on a limited pixel budget. This is a fundamental contemporary problem as there is a big gap between the resolutions of the capture and display devices, e.g. movies shot with professional cameras viewed on cell phones. Additionally, this algorithm allows us to downsample very high resolution imagery when the subsequent processing algorithms are not capable to handling the huge amount of data. 
Our algorithm also scales very well with image size, as it depends on a few standard convolutions, and provides significantly more accurate results.
See Fig.~\ref{perceptual} for some example results.


\begin{figure}[t]
	\centering
	\includegraphics[width=.99\linewidth]{images/perceptual-teaser}
	\caption{In \cite{Oztireli15Perceptual} we proposed a method to reproduce a perceptually similar downscaled version of an image. Compared to simple subsampling, the
		commonly used bicubic filter, and the state-of-the-art method of Kopf et al. [2013] (“Content-Adaptive”), our method (“Perceptual”) relies on a perceptual image
		quality measure instead of standard metrics, and is able to preserve perceptually important features and the overall
		look of the original images. Left input image courtesy of Flickr user Matteo Catanese.
		}
	\label{perceptual}
\end{figure} 






\subsection{WP 3: Joint Correspondence Analysis and Image Synthesis} % WP3
%Methods that do not consider correspondence estimation in the form of optical flow or stereo depth maps separately but in a joint framework.

While we could show that capturing a dense light field enables very accurate depth estimation results \cite{KimZPSG13}, the sheer amount of data can make this cumbersome for large scale deployment.
In \cite{KimSMSG15} we hence proposed an online method to optimally sample the viewing positions to obtain accurate depth estimation results from a minimal set of views. This is achieved by optimizing the trade-off between visibility and depth resolvability for varying sampling locations to best balance the two conflicting criteria. In our online algorithm we successively compute depth and then estimate the quality of the current estimation, which essentially comes down to evaluating its interpolation capabilities. See Fig.~\ref{sampling} for an example.

\begin{figure}[t]
	\centering
	\includegraphics[width=\linewidth]{images/sampling}
	\caption{In \cite{KimSMSG15} we proposed a method that allows us to reduce the number of input views required for our light field depth estimation framework \cite{KimZPSG13}. To this end we find the optimal sampling positions by jointly analyzing intermediate depth estimates and their interpolation quality. This allows us to obtain better depth estimates compared to our previous regular sampling.}
	\label{sampling}
\end{figure}


in \cite{KimMZPSG14} we presented an efficient method to generate stereoscopic image pairs form light field input. Here the desired depth is given by the user for a reference image and by analyzing the depth correspondences we interpolate an optimal second image to form a stereo pair from the captured or rendered light field data. The basic idea for this was first proposed in previous work (\textit{Kim et al., Multi-Perspective Stereoscopy from Light Fields, SIGGRAPH Asia 2011}) but this approach was very memory intensive, making it impossible to process HD footage that is common in today's movie productions. By casting the problem in a novel continuous energy optimization framework we obtained a reduction of the memory footprint by one order of magnitude, allowing us to easily process HD footage on standard PCs as shown in Fig.~\ref{stereo}.

\begin{figure}[t]
	\centering
	\includegraphics[width=.65\linewidth]{images/stereo-compare}
	\hspace*{5mm}
	\includegraphics[width=.29\linewidth]{images/stereo-mem}
	\caption{In \cite{KimMZPSG14} we presented an efficient method to generate stereoscopic view from light field input  (left images, top row). It produces comparable results to previous work (\textit{Kim et al., Multi-Perspective Stereoscopy from Light Fields, SIGGRAPH Asia 2011}) (left images, bottom row), but is much more memory efficient, allowing us to easily handle HD footage (right).}
	\label{stereo}
\end{figure}


%\subsection{Relaxed Interpolation Techniques} % WP4
%???

\subsection{New WP: Video Segmentation}
We found that video segmentation can be a very important building block for high quality video interpolation tasks. Delineating foreground objects from the background allows us to better reason about occlusion boundaries and eventually might help to overcome the main remaining problem of interpolation techniques that is how to appropriately handle occlusions. We thus investigated novel, efficient video segmentation algorithms.

In \cite{PerazziSS15} we presented a very efficient state-of-the-art method to detect the salient foreground pixels of a video; see Fig.\ \ref{saliency}. Based on the assumption that the colors of salient objects will typically differ from the colors on the image borders, we can obtain very good saliency estimates with a simple algorithm that analyzes the eigenvalues of a graph Laplacian that is defined over the color similarity of image superpixels. This algorithm is very efficient and requires less than a than a second for standard image resolutions.
Moreover, exploiting discriminative properties of the Fiedler vector, we devised an SVM-based classifier that allows us to determine whether an image contains any salient objects at all, a problem that has been largely neglected in previous works.
We also showed how the per-frame saliency estimates can be extended to improve spatio-temporal coherence using efficient high dimensional filtering.

\begin{figure}[t]
	\centering
	\includegraphics[width=.49\linewidth]{images/saliency-im}
	\includegraphics[width=.49\linewidth]{images/saliency-res}
	\caption{In \cite{PerazziSS15} we presented a very efficient, state-of-the-art saliency detection algorithm for videos. In the saliency map on the right, brighter pixels have a higher saliency.}
	\label{saliency}
\end{figure}

In \cite{PerazziWGS15} we considered the more general task of video segmentation based on object proposals.
Based on a very large number of object proposals for each video frame that are extracted using existing techniques we perform an SVM-based pruning step to retain only high quality proposals with sufficiently discriminative power. Finally, we determine the fore- and background segmentation by solving a fully connected conditional random field, defined using a novel energy function. We could show that combining object proposals from a whole video allows to outperform many state-of-the-art methods that are more localized. See Fig.\ \ref{fcop} for some example segmentation results.

\begin{figure}[t]
	\centering
	\includegraphics[width=\linewidth]{images/fcop_teaser}
	\caption{An example video segmentation result produced by our technique presented in \cite{PerazziWGS15}.}
	\label{fcop}
\end{figure}

While there are many accurate general purpose video segmentation algorithms have been proposed, many do not scale well to high resolution data.
In \cite{Maerki_CVPR_2016} we presented a video segmentation algorithm that is very efficient, even on high resolution video datasets; see Fig.\ \ref{bls}.
Our idea was to do video segmentation in \emph{bilateral space}. We design a new energy on the vertices of a regularly sampled spatio-temporal bilateral grid, which can be solved efficiently using a standard graph cut label assignment. Using a bilateral formulation, the energy that we minimize implicitly approximates long-range, spatio-temporal connections between pixels while still containing only a small number of variables and only local graph edges. Compared to a number of recent methods, our approach achieves state-of-the-art results on multiple benchmarks in a fraction of the runtime. Furthermore, our method scales linearly with image size, allowing for interactive feedback on real-world high resolution video.

\begin{figure}[t]
	\centering
	\includegraphics[width=.54\linewidth]{images/bls}
	\includegraphics[width=.45\linewidth]{images/bls-tab}
	\caption{Our bilateral space video segmentation method \cite{Maerki_CVPR_2016} achieves high quality segmentation results (left) in a fraction of the time compared to existing methods (right). Our method is BVS$_Q$ (quality setting) and BVS$_S$ (speed setting).}
	\label{bls}
\end{figure}



\subsection{WP 5: Parallelization and GPU-based Implementation} % WP5
For all parts of this project we designed the algorithms in such a way that they can be efficiently parallelized either on GPUs or on compute clusters. This is essential to make the resulting methods practical for use in production environments.

For our work on producing panoramic video from large, unstructured camera arrays \cite{PerazziSZKWWG15} we designed the method in a way so that each frame of the output panoramic video is computed independently as jointly computing the solution for a large video volume is unpractical both in terms of computation time and memory. Thus our method can easily be parallelized by computing each frame independently on a large compute cluster (ETH Zurich's Brutus cluster).

In our work on depth estimation from densely sampled light fields \cite{KimZPSG13} we refrained from a common global solution that is challenging to efficiently parallelize. Instead we showed that the high redundancy in densely sampled light fields allows us to obtain accurate results by performing per-pixel computations, making the algorithm easily parallelized on a GPU. Additionally, we can leverage the regular structure of the light field input to process the light field on a per-scanline basis, making it very memory efficient which is important due to the often limited memory resources of GPUs.

Also our recently proposed point cloud denoising method that improves the results of our light field depth estimation technique \cite{KimZPSG13} can process the input from each pixel of each depth map independently, making it an ideal candidate for a parallel GPU implementation. If several GPUs are available it will even be possible to process each depth map in parallel. After this pre-processing step, the final gathering step is very efficient and computationally negligible.

Our video saliency method \cite{PerazziSS15} can be run for each frame independently on several CPU cores or even on a cluster. The additional temporal smoothing step uses efficient inference in
fully connected conditional random fields (\textit{Kr\"ahenb\"uhl and Koltun, NIPS 2011}) and has been implemented on a GPU.

For our work on video segmentation based on fully connected object proposals \cite{PerazziWGS15}, most of the components of our algorithm are parallelizable. Those that are not, such as the multi-dimensional scaling, are relatively efficient.

While being already very efficient in a sequential implementation, our bilateral space video segmentation algorithm \cite{Maerki_CVPR_2016} can be parallelized for most parts:  The two most expensive steps (lifting to bilateral space and slicing) can be trivially parallelized since their output values only depends on color and position of individual pixels. Splatting to project back to image space can also be performed on concurrent threads, simply augmenting the grid with a small number of accumulators at bilateral vertices. The only stage that is not easily parallelizable is graph-cut, which anyway has small runtime due to the size and sparsity of the bilateral grid.


\subsection{WP 6: Benchmark Design and Construction} %WP6:
\begin{figure}[t]
	\centering
	\includegraphics[width=\linewidth]{images/dflf-data}
	\caption{Some of the datasets we captured in the context of our work on depth estimation from densely sampled light fields \cite{KimZPSG13}. We made the high resolution input data as well as our computed depth maps available to the research community (\texttt{http://www.disneyresearch.com/project/lightfields/}).}
	\label{dflf-data}
\end{figure}

\begin{figure}[t]
	\centering
	\includegraphics[width=\linewidth]{images/davis_teaser}
	\caption{Some example frames from the 50 videos of our \textit{DAVIS} video segmentation benchmark \cite{Perazzi_CVPR_2016} with overlaid ground truth masks (\texttt{https://graphics.ethz.ch/\textasciitilde perazzif/davis/index.html}).}
	\label{davis}
\end{figure}

%\subsubsection{Light Field Dataset for Depth Estimation}
In the context of our work on depth estimation from densely sampled light fields \cite{KimZPSG13} we needed to generate novel data sets as until then such data was not available to the research community.
We captured several indoor and outdoor data sets (3D light fields) by mounting a consumer DSLR
camera on a motorized linear stage; see Fig.\ \ref{dflf-data} for some examples and the corresponding depth maps we computed. The camera was a Canon EOS 5D Mark II with a 50 mm lens with which we captured images at various resolutions up to 21 MP. The linear stage was a Zaber T-LST1500D that is 1.5 meter long and can be controlled from a computer to obtain an accurate spacing of camera positions. We captured 100 images of each scene with uniform spacing between the camera positions and used them for reconstruction. The spacing between camera positions ranges from 2 mm to 15 mm.
We made all the input data, pre-processed images (radially undistorted, rectified, and cropped), calibration information as well as our computed depth maps available to the research community (\texttt{http://www.disneyresearch.com/project/lightfields/}) to allow future works to use high quality data an compare to our depth estimation method.

%\subsubsection{Video Segmentation Dataset and Benchmark}
During our work on video segmentation we realized that while several standard datasets with manually annotated ground truth segmentation exist, they do not represent the kind of footage and the quality standards that is common in production scenarios.
We thus designed, captured and annotated on a new benchmark dataset and evaluation methodology for video segmentation \cite{Perazzi_CVPR_2016}. The dataset, named DAVIS (Densely Annotated VIdeo Segmentation), consists of fifty high quality, Full HD video sequences, spanning multiple occurrences of common video segmentation challenges such as occlusions, motion-blur and appearance changes; see Fig.\ \ref{davis} for some examples and \texttt{https://graphics.ethz.ch/\textasciitilde perazzif/davis/index.html} for the full benchmark. Each video is accompanied by densely annotated, pixel-accurate and per-frame ground truth segmentation. In addition, we provide a comprehensive analysis of several state-of-the-art segmentation approaches using three complementary metrics that measure the spatial extent of the segmentation, the accuracy of the silhouette contours and the temporal coherence. The results uncover strengths and weaknesses of current approaches, opening up promising directions for future works.


\subsection{WP7: Evaluation}

%\subsubsection{Light Field Dataset for Depth Estimation}
We compared our results for depth estimation from densely sampled light fields \cite{KimZPSG13} to existing methods that mostly rely on global optimization (e.g.\ graph cuts). The main insight was that these established methods do not scale well to large resolution data that can easily be acquired nowadays an that is already used in production scenarios. On the one hand, some methods simply cannot process the data due to memory requirements or take up to several hours to compute a single result. Even worse, these algorithms also do not achieve optimal results as they are not designed to leverage the high redundancy in the large amount of data. With our method we showed how simple algorithms can achieve highly accurate results from densely sampled data and hope that the dissemination of our input data together with our computed depth maps will spark more research in this interesting direction.
Note that as we cannot obtain ground truth depth maps for our light field data set we did not extend it to a full benchmark but only provide our depth maps for comparison.

%\subsubsection{Video Segmentation Dataset and Benchmark}
The evaluation results in our video segmentation benchmark
\cite{Perazzi_CVPR_2016} clearly show that both the aggregate and individual performance of state-of-the-art approaches still leave abundant room for future research. 
For instance, in the Geoesic Object Proposal work (\textit{Kr\"ahenb\"uhl and Koltun, ECCV 2014}) it is observed that an accuracy index bigger than 0.7 seems to be sufficiently precise for most applications, while results lower than 0.6 already represent a significant departure from the original object shape, rendering results not directly usable in production scenarios. The top techniques evaluated on DAVIS only obtain accuracy indexes around 0.6, proving that there is a need for improvement in terms of accuracy.
Also currently running time efficiency and memory requirements are a major bottleneck for the practical usability of several video segmentation algorithms. In our experiments we observed that a substantial amount of time is spent preprocessing images to extract boundary preserving regions, object proposals and motion estimates. We thus believe that future research should carefully select those components bearing in mind they could compromise the practical utility of their work. Efficient algorithms will be able to take advantage of the Full HD videos and accurate segmentation masks made available with our dataset. While leveraging high resolution data might not produce better results in terms of region-similarity, it is essential to improve the segmentation of complex object contours and tiny object regions.

While we already provide all the data including ground truth masks as well a code for performing evaluations online, we did not implement an online evaluation platform for the DAVIS benchmark yet due to time restrictions.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Contributions of the Project Staff}
Prof. Markus Gross contributed to the project by providing the full infrastructure. Changil Kim, Federico Perazzi, and Cengiz Oztireli contributed to the project by developing the technical ideas and methods, and implementing the resulting solutions in accordance with the task descriptions of the project.

\section{Research Output}
The main output of our research efforts are 10 peer-reviewed research papers that have been presented at leading graphics \cite{KimZPSG13,PerazziSZKWWG15,PerazziSS15,Oztireli15Perceptual} and vision conferences \cite{KimMZPSG14,KimSMSG15,PerazziWGS15,Maerki_CVPR_2016,Perazzi_CVPR_2016,WolffKZSBSS16}.

In addition we filed or are in the process of filing patent applications for all inventions that were made in the context of the published papers (apart from \cite{Oztireli15Perceptual} and \cite{Perazzi_CVPR_2016}). These are joint intellectual property of ETH Zurich and The Walt Disney Company which was a partner on these projects.

Having Disney as a partner also allowed us to actually use some of the developed techniques in actual production scenarios in the movie industry as well as in the context of creating footage for an upcoming theme park attraction.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\bibliographystyle{plain}
\bibliography{interpolation}
\end{document}
